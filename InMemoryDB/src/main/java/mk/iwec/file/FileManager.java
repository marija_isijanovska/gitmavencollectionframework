package mk.iwec.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import mk.iwec.model.Student;

public class FileManager {

	public static Map<Integer, Student> readFromDB(String database) {

		Map<Integer, Student> students = new HashMap<>();
		String line = "";
		String firstname = "";
		String lastname = "";
		try (BufferedReader br = new BufferedReader(new FileReader(database))) {
			while ((line = br.readLine()) != null) {
				StringTokenizer token = new StringTokenizer(line, " ");

				if (token.hasMoreTokens()) {
					firstname = token.nextToken();
				}
				if (token.hasMoreTokens()) {
					lastname = token.nextToken();
				}
				Student st = new Student(firstname, lastname);
				students.put(students.size() + 1, st);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return students;

	}
	
	public static boolean writeToFile(String file,Student st) {
		boolean fileIsOk = false;
		try (Writer w = new BufferedWriter(new FileWriter(file,true))){
			w.write(st.getFirstname()+" "+st.getLastname()+"\n");
			fileIsOk=true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	return fileIsOk;
	}
}
