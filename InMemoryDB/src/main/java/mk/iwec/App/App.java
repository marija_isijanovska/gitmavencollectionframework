package mk.iwec.App;

import java.util.List;

import mk.iwec.DAO.StudentDAO;
import mk.iwec.DAO.StudentDAOImpl;
import mk.iwec.file.FileManager;
import mk.iwec.model.Student;

public class App {

	public static void main(String[] args) {
        //List<Student>db = new FileManager().readFromDB(String.join(" ",args));
		StudentDAO<Student> st = new StudentDAOImpl();
        
		System.out.println("Affected:" + st.insert(new Student("Simona", "Ristevska")));
		System.out.println("Affected:" + st.insert(new Student("Tome", "Dimitrovski")));
		System.out.println(st.findAll());
		System.out.println("Updated:" + st.update(1, new Student("Ivana", "Torkovska")));
		System.out.println(st.findAll());
		System.out.println(st.findById(2));
		System.out.println("Deleted:" + st.deleteById(1));
		System.out.println(st.findAll());
		}

}
