package mk.iwec.model;

import lombok.Data;
import lombok.ToString;

import lombok.AllArgsConstructor;

@Data
@ToString
@AllArgsConstructor

public class Student {

	private String firstname;
	private String lastname;

}
