package mk.iwec.DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import mk.iwec.model.Student;

public interface StudentDAO<T> {

	public Optional<Map.Entry<Integer,T>> findById(Integer id);

	public Map<Integer,Student> findAll();

	public int insert(T o);

	public int update(Integer id,T o);

	public int deleteById(Integer id);

}
